# Solucionario
# Restricción primary key

# EJERCICIO 01
# Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".
## Elimine la tabla:

```sql
drop table empleados;

```
## Créela con la siguiente estructura:

```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20)
);

```
## Ingrese algunos registros, dos de ellos con el mismo número de documento:

```sql
INSERT INTO empleados VALUES ('22333444', 'Juan Perez', 'Administración');
INSERT INTO empleados VALUES ('23444555', 'Ana Acosta', 'Ventas');
INSERT INTO empleados VALUES ('22333444', 'Pedro Gomez', 'Recursos Humanos');
```
## Intente establecer una restricción "primary key" para la tabla para que el documento no se repita ni admita valores nulos. No lo permite porque la tabla contiene datos que no cumplen con la restricción, debemos eliminar (o modificar) el registro que tiene documento duplicado.

```sql
ALTER TABLE empleados ADD CONSTRAINT pk_documento_empleados PRIMARY KEY (documento);
```
## Establecezca la restricción "primary key" del punto 4

```sql
ALTER TABLE empleados ADD CONSTRAINT pk_empleados PRIMARY KEY (documento);
```
## Intente actualizar un documento para que se repita. No lo permite porque va contra la restricción.

```sql
UPDATE empleados SET documento = '22333444' WHERE nombre = 'Ana Acosta';
```
## Intente establecer otra restricción "primary key" con el campo "nombre".

```sql
ALTER TABLE empleados ADD CONSTRAINT pk_nombre_empleados PRIMARY KEY (nombre);

```
## Vea las restricciones de la tabla "empleados" consultando el catálogo "user_constraints" (1 restricción "P")

```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'EMPLEADOS';
```
## Consulte el catálogo "user_cons_columns"

```sql
SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'EMPLEADOS';
```
##  EJERCICIO 02
# Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".
## Elimine la tabla:

```sql
drop table remis;
```
## Cree la tabla con la siguiente estructura:

```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);

```
## Ingrese algunos registros sin repetir patente y repitiendo número.

```sql
INSERT INTO remis VALUES (10001, 'ABC123', 'Ford', '2021');
INSERT INTO remis VALUES (10001, 'DEF456', 'Chevrolet', '2022');
INSERT INTO remis VALUES (10002, 'GHI789', 'Renault', '2020');
```
## Ingrese un registro con patente nula.

```sql
INSERT INTO remis VALUES (10003, NULL, 'Volkswagen', '2019');

```
## Intente definir una restricción "primary key" para el campo "numero".

```sql
ALTER TABLE remis ADD CONSTRAINT pk_numero_remis PRIMARY KEY (numero);

```
## Intente establecer una restricción "primary key" para el campo "patente".

```sql
ALTER TABLE remis ADD CONSTRAINT pk_patente_remis PRIMARY KEY (patente);
```
## Modifique el valor "null" de la patente.

```sql
UPDATE remis SET patente = 'JKL012' WHERE numero = 10003;

```
## Establezca una restricción "primary key" del punto 6.

```sql
ALTER TABLE remis ADD CONSTRAINT pk_remis PRIMARY KEY (patente);

```
## Vea la información de las restricciones consultando "user_constraints" (1 restricción "P")

```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'REMIS';
```
## Consulte el catálogo "user_cons_columns" y analice la información retornada (1 restricción)

```sql
SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'REMIS';
```