# Solucionario 
# Disparador de actualizacion - campos (updating)

## Ejercicios propuestos

Un comercio almacena los datos de los artículos que tiene para la venta en una tabla denominada "articulos". En otra tabla denominada "pedidos" almacena el código de cada artículo y la cantidad que necesita solicitar a los mayoristas. En una tabla llamada "controlPrecios" almacena la fecha, el código del artículo y ambos precios (antiguo y nuevo).

1. Elimine las tablas:

```sql
drop table articulos;
drop table pedidos;
drop table controlPrecios;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table articulos(
    codigo number(4),
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4)
);

create table pedidos(
    codigo number(4),
    cantidad number(4)
);

create table controlPrecios(
    fecha date,
    codigo number(4),
    anterior number(6,2),
    nuevo number(6,2)
);
```

3. Ingrese algunos registros en "articulos":

```sql
insert into articulos values(100,'cuaderno rayado 24h',4.5,100);
insert into articulos values(102,'cuaderno liso 12h',3.5,150);
insert into articulos values(104,'lapices color x6',8.4,60);
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas xxx',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);
```

4. Ingrese en "pedidos" todos los códigos de "articulos", con "cantidad" cero
```sql

```
5. Active el paquete "dbms_output":

```sql
set serveroutput on;
execute dbms_output.enable(20000);
```

6. Cada vez que se disminuye el stock de un artículo de la tabla "articulos", se debe incrementar la misma cantidad de ese artículo en "pedidos" y cuando se incrementa en "articulos", se debe disminuir la misma cantidad en "pedidos". Si se ingresa un nuevo artículo en "articulos", debe agregarse un registro en "pedidos" con "cantidad" cero. Si se elimina un registro en "articulos", debe eliminarse tal artículo de "pedidos". Cree un trigger para los tres eventos (inserción, borrado y actualización), a nivel de fila, sobre "articulos", para los campos "stock" y "precio", que realice las tareas descriptas anteriormente, si el campo modificado es "stock". Si el campo modificado es "precio", almacene en la tabla "controlPrecios", la fecha, el código del artículo, el precio anterior y el nuevo.

El trigger muestra el mensaje "Trigger activado" cada vez que se dispara; en cada "if" muestra un segundo mensaje que indica cuál condición se ha cumplido.
```sql

```
7. Disminuya el stock del artículo "100" a 30
Un mensaje muestra que el trigger se ha disparado actualizando el "stock".
```sql

```
8. Verifique que el trigger se disparó consultando la tabla "pedidos" (debe aparecer "70" en "cantidad" en el registro correspondiente al artículo "100")
```sql

```
9. Ingrese un nuevo artículo en "articulos"
Un mensaje muestra que el trigger se ha disparado por una inserción.
```sql

```
10. Verifique que se ha agregado un registro en "pedidos" con código "280" y cantidad igual a 0
```sql

```
11. Elimine un artículo de "articulos"
Un mensaje muestra que el trigger se ha disparado por un borrado.
```sql

```
12. Verifique que se ha borrado el registro correspondiente al artículo con código "234" en "pedidos"
```sql

```
13. Modifique el precio de un artículo
Un mensaje muestra que el trigger se ha disparado por una actualización de precio.
```sql

```
14. Verifique que se ha agregado un registro en "controlPrecios"
```sql

```
15. Modifique la descripción de un artículo
El trigger no se ha disparado, no aparece mensaje.
```sql

```
16. Modifique el precio, stock y descripcion de un artículo
Un mensaje muestra que el trigger se ha disparado por una actualización de stock y otra de precio. La actualización de "descripcion" no disparó el trigger.
```sql

```
17. Verifique que se ha agregado un registro en "controlPrecios" y se ha modificado el campo "cantidad" con el valor "5"
```sql

```
18. Modifique el stock de varios artículos en una sola sentencia
Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por actualizaciones de stock.
```sql

```
19. Verifique que se han modificado 4 registros en "pedidos"
```sql

```
20. Modifique el precio de varios artículos en una sola sentencia
Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por actualizaciones de precio.
```sql

```
21. Verifique que se han agregado 4 nuevos registros en "controlPrecios"
```sql

```
22. Elimine varios artículos en una sola sentencia
Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por borrado de registros.
```sql

```
23. Verifique que se han eliminado 4 registros en "pedidos"
```sql

```