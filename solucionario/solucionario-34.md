# Solucionario 
# Secuencias (create sequence - currval - nextval - drop sequence)

## EJERCICIO 01
# Una empresa registra los datos de sus empleados en una tabla llamada "empleados".
## Elimine la tabla "empleados":

```sql
drop table empleados;
```
## 02 Cree la tabla:

```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```
## Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (999), valor inicial (100), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.


```sql
DROP SEQUENCE sec_legajoempleados;
```
```sql
CREATE SEQUENCE sec_legajoempleados
    START WITH 100
    INCREMENT BY 2
    MINVALUE 1
    MAXVALUE 999
    NO CYCLE;
```
## Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria:

```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

insert into empleados
values (sec_legajoempleados.nextval,'26777888','Estela Esper');

```
## Recupere los registros de "libros" para ver los valores de clave primaria.

```sql
SELECT * FROM libros;
```
## Vea el valor actual de la secuencia empleando la tabla "dual". Retorna 108.

```sql
SELECT sec_legajoempleados.currval FROM dual;

```
## Recupere el valor siguiente de la secuencia empleando la tabla "dual" Retorna 110.

```sql
SELECT sec_legajoempleados.currval FROM dual;
```
## Ingrese un nuevo empleado (recuerde que la secuencia ya tiene el próximo valor, emplee "currval" para almacenar el valor de legajo)

```sql
INSERT INTO empleados
VALUES (sec_legajoempleados.currval, '29000111', 'Hector Huerta');

``` 
## Recupere los registros de "libros" para ver el valor de clave primaria ingresado anteriormente.

```sql
SELECT * FROM libros;
```
## Incremente el valor de la secuencia empleando la tabla "dual" (retorna 112)

```sql
SELECT sec_legajoempleados.nextval FROM dual;
```
## Ingrese un empleado con valor de legajo "112".

```sql
INSERT INTO empleados
VALUES (112, '30000222', 'Isabel Iglesias');
```
## Intente ingresar un registro empleando "currval":

```sql
insert into empleados
values (sec_legajoempleados.currval,'29000111','Hector Huerta');
```
## Incremente el valor de la secuencia. Retorna 114.

```sql
SELECT sec_legajoempleados.nextval FROM dual;

```
## Ingrese el registro del punto 11.

```sql
INSERT INTO empleados
VALUES (112, '30000222', 'Isabel Iglesias');
```
## Recupere los registros.

```sql
SELECT * FROM empleados;
```
## Vea las secuencias existentes y analice la información retornada.

```sql
SELECT * FROM user_sequences;
```
## Vea todos los objetos de la base de datos actual que contengan en su nombre la cadena "EMPLEADOS". Debe aparacer la tabla "empleados" y la secuencia "sec_legajoempleados".

```sql
SELECT object_name, object_type FROM all_objects WHERE object_name LIKE '%EMPLEADOS%';
```
## Elimine la secuencia creada.

```sql
DROP SEQUENCE sec_legajoempleados;
```
## Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.

```sql
SELECT object_name, object_type FROM all_objects WHERE object_type = 'SEQUENCE';
```