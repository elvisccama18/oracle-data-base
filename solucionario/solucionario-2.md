# Solucionario  
# Ingresar registros (insert into- select)

# EJERCICIO 001
## Trabaje con la tabla "agenda" que almacena información de sus amigos.

## Elimine la tabla "agenda"
```sql
drop table agenda;
```
```sh
Table AGENDA dropped.
```
## Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11).
```sql
create table agenda(
    nombre varchar2(20),
    domicilio varchar2(30),
    apellido varchar2(30),
    telefono varchar2(11)
);
```
```sh
Table AGENDA created.
```
## Visualice las tablas existentes para verificar la creación de "agenda" (all_tables).
```sql
select * from all_tables;
```
```sh
si existe la tabla agenda
```
## Visualice la estructura de la tabla "agenda" (describe).

```sql
DESCRIBE agenda;
```
```sh
Name      Null? Type         
--------- ----- ------------ 
NOMBRE          VARCHAR2(20) 
DOMICILIO       VARCHAR2(30) 
TELEFONO        VARCHAR2(11) 
```
## Ingrese los siguientes registros:
```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');
```
```h
1 row inserted.
```
## Seleccione todos los registros de la tabla.
```sql
select * from agenda;
```
```sh
NOMBRE   APELLIDO  DOMICILIO       TELEFONO
Alberto  Moreno    Colon 123       4234567
Juan     Torrea    Avellanada123   4458787
```
## Elimine la tabla "agenda".
```sql
drop table agenda;
```
```sh
Table AGENDA dropped.
```
## Intente eliminar la tabla nuevamente (aparece un mensaje de error).
```sql
drop table agenda;
```
```sh
Error starting at line : 1 in command -
drop table agenda
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```


# EJERCICIO 002
## Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.

## Elimine la tabla "libros".
```sql
drop table libros;
```
```sh
Table LIBROS dropped.
```
## Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)
```sql
create table libros (
     titulo varchar2(20), 
     autor varchar2(30),  
     editorial varchar2(15)
);
```
```sh
Table LIBROS created.
```
## Visualice las tablas existentes.
```sql
select * from libros;
```
```sh
TITULO AUTOR EDITORIAL
```
## Visualice la estructura de la tabla "libros" Muestra los campos y los tipos de datos de la tabla "libros".
```sql
DESCRIBE LIBROS;
```
```sh
Name      Null? Type         
--------- ----- ------------ 
TITULO          VARCHAR2(20) 
AUTOR           VARCHAR2(30) 
EDITORIAL       VARCHAR2(15) 
```
## Ingrese los siguientes registros:
```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');
```
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## Muestre todos los registros (select) de "libros"
```sql
select * from libros;
```
```sh
TUTOR           AUTOR           EDITORIAL
El aleph        Borges          Planeta
Martin Fierro   Jose Hernandez  Emece
Aprenda PHP     Mario Molina    Emece
```