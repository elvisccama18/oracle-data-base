# Solucionario  
# Registros duplicados (Distinct)

# EJERCICIO 01
# Una empresa tiene registrados sus clientes en una tabla llamada "clientes".

## Elimine la tabla "clientes" y créela con la siguiente estructura:

```sql
 drop table clientes;

 create table clientes (
  nombre varchar2(30) not null,
  domicilio varchar2(30),
  ciudad varchar2(20),
  provincia varchar2(20)
);
```
## Ingrese algunos registros:

```sql
insert into clientes
values ('Lopez Marcos','Colon 111','Cordoba','Cordoba');

insert into clientes
values ('Perez Ana','San Martin 222','Cruz del Eje','Cordoba');

insert into clientes
values ('Garcia Juan','Rivadavia 333','Villa del Rosario','Cordoba');

insert into clientes
values ('Perez Luis','Sarmiento 444','Rosario','Santa Fe');

insert into clientes
values ('Pereyra Lucas','San Martin 555','Cruz del Eje','Cordoba');

insert into clientes
values ('Gomez Ines','San Martin 666','Santa Fe','Santa Fe');

insert into clientes
values ('Torres Fabiola','Alem 777','Villa del Rosario','Cordoba');

insert into clientes
values ('Lopez Carlos',null,'Cruz del Eje','Cordoba');

insert into clientes
values ('Ramos Betina','San Martin 999','Cordoba','Cordoba');

insert into clientes
values ('Lopez Lucas','San Martin 1010','Posadas','Misiones');

```
## Obtenga las provincias sin repetir (3 registros)

```sql
SELECT DISTINCT provincia
FROM clientes;
```
## Cuente las distintas provincias (retorna 3)

```sql
SELECT COUNT(DISTINCT provincia) AS cantidad_provincias
FROM clientes;
```
## Se necesitan los nombres de las ciudades sin repetir (6 registros)

```sql
SELECT DISTINCT ciudad
FROM clientes;
```
## Obtenga la cantidad de ciudades distintas (devuelve 6)

```sql
SELECT COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM clientes;
```
## Combine con "where" para obtener las distintas ciudades de la provincia de Cordoba (3 registros)

```sql
SELECT DISTINCT ciudad
FROM clientes
WHERE provincia = 'Cordoba';
```
## Contamos las distintas ciudades de cada provincia empleando "group by" (3 filas

```sql
SELECT provincia, COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM clientes
GROUP BY provincia;
```
## EJERCICIO 02
# La provincia almacena en una tabla llamada "inmuebles" los siguientes datos de los inmuebles y sus propietarios para cobrar impuestos:
## Elimine la tabla:

```sql
drop table inmuebles;
```
## Créela con la siguiente estructura:

```sql
create table inmuebles (
    documento varchar2(8) not null,
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(20),
    barrio varchar2(20),
    ciudad varchar2(20),
    tipo char(1),..b=baldio, e: edificado
    superficie number(8,2)
);
```
## Ingrese algunos registros:

```sql
insert into inmuebles
values ('11000000','Perez','Alberto','San Martin 800','Centro','Cordoba','e',100);

insert into inmuebles
values ('11000000','Perez','Alberto','Sarmiento 245','Gral. Paz','Cordoba','e',200);

insert into inmuebles
values ('12222222','Lopez','Maria','San Martin 202','Centro','Cordoba','e',250);

insert into inmuebles
values ('13333333','Garcia','Carlos','Paso 1234','Alberdi','Cordoba','b',200);

insert into inmuebles
values ('13333333','Garcia','Carlos','Guemes 876','Alberdi','Cordoba','b',300);

insert into inmuebles
values ('14444444','Perez','Mariana','Caseros 456','Flores','Cordoba','b',200);

insert into inmuebles
values ('15555555','Lopez','Luis','San Martin 321','Centro','Carlos Paz','e',500);

insert into inmuebles
values ('15555555','Lopez','Luis','Lopez y Planes 853','Flores','Carlos Paz','e',350);

insert into inmuebles
values ('16666666','Perez','Alberto','Sucre 1877','Flores','Cordoba','e',150);
```
## Muestre los distintos apellidos de los propietarios, sin repetir (3 registros)

```sql
SELECT DISTINCT apellido
FROM inmuebles;
```
## Recupere los distintos documentos de los propietarios y luego muestre los distintos documentos de los propietarios, sin repetir y vea la diferencia (9 y 6 registros respectivamente)

```sql
SELECT documento
FROM inmuebles
GROUP BY documento;

SELECT DISTINCT documento
FROM inmuebles;

```
## Cuente, sin repetir, la cantidad de propietarios de inmuebles de la ciudad de Cordoba (5)

```sql
SELECT COUNT(DISTINCT documento) AS cantidad_propietarios
FROM inmuebles
WHERE ciudad = 'Cordoba';
```
## Cuente la cantidad de inmuebles con domicilio en 'San Martin' (3)

```sql
SELECT COUNT(*) AS cantidad_inmuebles
FROM inmuebles
WHERE domicilio = 'San Martin';
```
## Cuente la cantidad de inmuebles con domicilio en 'San Martin', sin repetir la ciudad (2 registros). Compare con la sentencia anterior.

```sql
SELECT COUNT(DISTINCT ciudad) AS cantidad_inmuebles
FROM inmuebles
WHERE domicilio = 'San Martin';
```
## Muestre los apellidos y nombres de todos los registros(9 registros)

```sql
SELECT apellido, nombre
FROM inmuebles;
```
## Muestre los apellidos y nombres, sin repetir (5 registros) Note que si hay 2 personas con igual nombre y apellido aparece una sola vez.

```sql
SELECT DISTINCT apellido, nombre
FROM inmuebles
LIMIT 5;
```
## Muestre la cantidad de inmuebles que tiene cada propietario en barrios conocidos, agrupando por documento (6 registros)

```sql
SELECT documento, COUNT(*) AS cantidad_inmuebles
FROM inmuebles
WHERE barrio <> ''
GROUP BY documento;
```
## Realice la misma consulta anterior pero en esta oportunidad, sin repetir barrio (6 registros) Compare los valores con los obtenidos en el punto 11.

```sql
SELECT documento, COUNT(DISTINCT barrio) AS cantidad_barrios
FROM inmuebles
WHERE barrio <> ''
GROUP BY documento;
```

