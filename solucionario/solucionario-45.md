# Solucionario
#

## Ejercicios propuestos
# Un profesor guarda algunos datos de sus alumnos en una tabla llamada "alumnos". 
## 01 Elimine la tabla y créela con la siguiente estructura:

```sql
drop table alumnos;

create table alumnos(
    legajo char(5) not null,
    documento char(8) not null,
    nombre varchar2(30),
    curso char(1) not null,
    materia varchar2(20) not null,
    notafinal number(4,2)
);

```
## Cree un índice no único para el campo "nombre".

```sql
CREATE INDEX idx_nombre ON alumnos(nombre);

```
## Establezca una restricción "primary key" para el campo "legajo"

```sql
ALTER TABLE alumnos ADD CONSTRAINT PK_alumnos_legajo PRIMARY KEY (legajo);

```
## Verifique que se creó un índice con el nombre de la restricción

```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';
```
## Verifique que se creó un índice único con el nombre de la restricción consultando el diccionario de índices.

```sql
SELECT index_name, uniqueness
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';
```
## Intente eliminar el índice "PK_alumnos_legajo" con "drop index".

```sql
DROP INDEX PK_alumnos_legajo;
```
## Cree un índice único para el campo "documento".

```sql
CREATE UNIQUE INDEX idx_documento ON alumnos(documento);
```
## Agregue a la tabla una restricción única sobre el campo "documento" y verifique que no se creó un índice, Oracle emplea el índice creado en el punto anterior.

```sql
ALTER TABLE alumnos ADD CONSTRAINT UK_alumnos_documento UNIQUE (documento);

```
## Intente eliminar el índice "I_alumnos_documento" (no se puede porque una restricción lo está utilizando)

```sql
DROP INDEX idx_documento;
```
## Elimine la restricción única establecida sobre "documento".

```sql
ALTER TABLE alumnos DROP CONSTRAINT UK_alumnos_documento;

```
## Verifique que el índice "I_alumnos_documento" aún existe.

```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'I_ALUMNOS_DOCUMENTO';
```
## Elimine el índice "I_alumnos_documento", ahora puede hacerlo porque no hay restricción que lo utilice.

```sql
DROP INDEX I_alumnos_documento;

```
## Elimine el índice "I_alumnos_nombre".

```sql
DROP INDEX idx_nombre;

```
## Elimine la restricción "primary key"/

```sql
ALTER TABLE alumnos DROP CONSTRAINT PK_alumnos_legajo;

``` 
## Verifique que el índice "PK_alumnos_legajo" fue eliminado (porque fue creado por Oracle al establecerse la restricción)

```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';
```
## Cree un índice compuesto por los campos "curso" y "materia", no único.

```sql
CREATE INDEX idx_curso_materia ON alumnos(curso, materia);

```
## Verifique su existencia.

```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'IDX_CURSO_MATERIA';
```
## Elimine la tabla "alumnos" y verifique que todos los índices han sido eliminados junto con ella.

```sql
DROP TABLE alumnos;

SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS';
```