# Solucionario.
# Indices (Crear . Información).

## Ejercicios de laboratorio
# Una playa de estacionamiento almacena cada día los datos de los vehículos que ingresan en la tabla llamada "vehiculos".
## Setee el formato de "date" para que nos muestre hora y minutos:

```sql
alter SESSION SET NLS_DATE_FORMAT = 'HH24:MI';
```
##  Elimine la tabla y créela con la siguiente estructura:

```sql
drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date not null,
    horasalida date
);

```
## Establezca una restricción "check" que admita solamente los valores "a" y "m" para el campo "tipo":

```sql
alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));
```
## Agregue una restricción "primary key" que incluya los campos "patente" y "horallegada"

```sql
ALTER TABLE vehiculos ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
## Ingrese un vehículo.

```sql
INSERT INTO vehiculos VALUES ('ABC123', 'a', SYSDATE, NULL);

```
## Intente ingresar un registro repitiendo la clave primaria.

```sql
INSERT INTO vehiculos VALUES ('ABC123', 'a', SYSDATE, NULL); -- Generará un error debido a la violación de la clave primaria

```
## Ingrese un registro repitiendo la patente pero no la hora de llegada.

```sql
INSERT INTO vehiculos VALUES ('ABC123', 'm', SYSDATE + 1/24/60, NULL);

```
## Ingrese un registro repitiendo la hora de llegada pero no la patente.

```sql
INSERT INTO vehiculos VALUES ('XYZ789', 'a', SYSDATE + 1/24/60, NULL);

```
## Vea todas las restricciones para la tabla "vehiculos" aparecen 4 filas, 3 correspondientes a restricciones "check" y 1 a "primary key". Dos de las restricciones de control tienen nombres dados por Oracle.

```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
## Elimine la restricción "primary key"

```sql
ALTER TABLE vehiculos DROP CONSTRAINT PK_vehiculos;

```
## Vea si se ha eliminado. Ahora aparecen 3 restricciones.

```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
## Elimine la restricción de control que establece que el campo "patente" no sea nulo (busque el nombre consultando "user_constraints").

```sql
ALTER TABLE vehiculos DROP CONSTRAINT SYS_C0011546;

```
## Vea si se han eliminado.

```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
## Vuelva a establecer la restricción "primary key" eliminada.

```sql
ALTER TABLE vehiculos ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
## La playa quiere incluir, para el campo "tipo", además de los valores permitidos "a" (auto) y "m" (moto), el caracter "c" (camión). No puede modificar la restricción, debe eliminarla y luego redefinirla con los 3 valores.

```sql
ALTER TABLE vehiculos DROP CONSTRAINT CK_vehiculos_tipo;
ALTER TABLE vehiculos ADD CONSTRAINT CK_vehiculos_tipo CHECK (tipo IN ('a', 'm', 'c'));
```
## Consulte "user_constraints" para ver si la condición de chequeo de la restricción "CK_vehiculos_tipo" se ha modificado.

```sql
SELECT constraint_name, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS' AND constraint_name = 'CK_VEHICULOS_TIPO';
```