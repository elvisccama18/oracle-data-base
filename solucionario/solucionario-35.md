# Solucionario 
# Alterar secuencia (alter sequence).


# EJERCICIO 01.
## Una empresa registra los datos de sus empleados en una tabla llamada "empleados".
## Elimine la tabla "empleados":

```sql
drop table empleados;
```
## Cree la tabla:

```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```
## Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (210), valor inicial (206), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.

```sql
DROP SEQUENCE sec_legajoempleados;
CREATE SEQUENCE sec_legajoempleados
    MINVALUE 1
    MAXVALUE 210
    START WITH 206
    INCREMENT BY 2
    NOCYCLE;
```
## Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria.

```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

```
## Recupere los registros de "libros" para ver los valores de clave primaria.

```sql
SELECT * FROM empleados;

```
## Vea el valor actual de la secuencia empleando la tabla "dual"

```sql
SELECT sec_legajoempleados.CURRVAL FROM dual;

```
## Intente ingresar un registro empleando "nextval":

```sql
insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');
```
## Altere la secuencia modificando el atributo "maxvalue" a 999.

```sql
ALTER SEQUENCE sec_legajoempleados MAXVALUE 999;
```
## Obtenga información de la secuencia.

```sql
SELECT * FROM user_sequences WHERE sequence_name = 'SEC_LEGAJOEMPLEADOS';

```
## Ingrese el registro del punto 7.

```sql
INSERT INTO empleados
VALUES (sec_legajoempleados.NEXTVAL, '25666777', 'Diana Dominguez');
```
## Recupere los registros.

```sql
SELECT * FROM empleados;
```
## Modifique la secuencia para que sus valores se incrementen en 1.

```sql
ALTER SEQUENCE sec_legajoempleados INCREMENT BY 1;
```
## Ingrese un nuevo registro:

```sql
insert into empleados
values (sec_legajoempleados.nextval,'26777888','Federico Fuentes');

```
## Recupere los registros.

```sql
SELECT * FROM empleados;
```
## Elimine la secuencia creada.

```sql
DROP SEQUENCE sec_legajoempleados;
```
## Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.

```sql
SELECT object_name, object_type FROM all_objects WHERE object_type = 'SEQUENCE';
```