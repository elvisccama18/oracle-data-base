# Solucionario.
# Restricciones: validación y estados (validate - novalidate - enable - disable).

## Ejercicios propuestos
# Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".
## Elimine la tabla:

```sql
drop table empleados;
```
## Créela con la siguiente estructura e ingrese los registros siguientes:

```sql
create table empleados (
    codigo number(6),
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2)
);

insert into empleados
values (1,'22222222','Alberto Acosta','Sistemas',-10);

insert into empleados
values (2,'33333333','Beatriz Benitez','Recursos',3000);

insert into empleados
values (3,'34444444','Carlos Caseres','Contaduria',4000);

```
## Intente agregar una restricción "check" para asegurarse que no se ingresen valores negativos para el sueldo sin especificar validación ni estado:

```sql
alter table empleados
add constraint CK_empleados_sueldo_positivo
check (sueldo>=0);

```
## Vuelva a intentarlo agregando la opción "novalidate".

```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_sueldo_positivo CHECK (sueldo >= 0) NOVALIDATE;

```
## ntente ingresar un valor negativo para sueldo.

```sql
INSERT INTO empleados VALUES (4, '55555555', 'David Delgado', 'Ventas', -1000);

```
## Deshabilite la restricción e ingrese el registro anterior.

```sql
ALTER TABLE empleados DISABLE CONSTRAINT CK_empleados_sueldo_positivo;

INSERT INTO empleados VALUES (4, '45555555', 'David Duran', 'Ventas', -2000);

```
## Intente establecer una restricción "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contaduría" sin especificar validación:

```sql
alter table empleados
add constraint CK_empleados_seccion_lista
check (seccion in ('Sistemas','Administracion','Contaduria'));
```
## Establezca la restricción anterior evitando que se controlen los datos existentes.

```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_seccion_lista CHECK (seccion IN ('Sistemas', 'Administracion', 'Contaduria')) NOVALIDATE;

```
## Vea si las restricciones de la tabla están o no habilitadas y validadas. Muestra 2 filas, una por cada restricción; ambas son de control, ninguna valida los datos existentes, "CK_empleados_sueldo_positivo" está deshabilitada, la otra habilitada.

```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
## Habilite la restricción deshabilitada.Note que existe un sueldo que infringe la condición.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT CK_empleados_sueldo_positivo;

```
## Intente modificar la sección del empleado "Carlos Caseres" a "Recursos" No lo permite.

```sql
UPDATE empleados SET seccion = 'Recursos' WHERE nombre = 'Carlos Caseres';

```
## Deshabilite la restricción para poder realizar la actualización del punto precedente.

```sql
ALTER TABLE empleados DISABLE CONSTRAINT CK_empleados_seccion_lista;

UPDATE empleados SET seccion = 'Recursos' WHERE nombre = 'Carlos Caseres';

```
## Agregue una restricción "primary key" para el campo "codigo" deshabilitada.

```sql
ALTER TABLE empleados ADD CONSTRAINT PK_empleados_codigo PRIMARY KEY (codigo) DISABLE;

```
## Ingrese un registro con código existente.

```sql
INSERT INTO empleados VALUES (1, '77777777', 'Eduardo Escobar', 'Ventas', 5000);

```
## Intente habilitar la restricción. No se permite porque aun cuando se especifica que no lo haga, Oracle verifica los datos existentes, y existe un código repetido.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo NOVALIDATE;

```
## Modifique el registro con clave primaria repetida.

```sql
UPDATE empleados SET codigo = 4 WHERE codigo = 1;

```
## Habilite la restricción "primary key"

```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo;

```
## Agregue una restricción "unique" para el campo "documento"

```sql
ALTER TABLE empleados ADD CONSTRAINT UQ_empleados_documento UNIQUE (documento);

```
## Vea todas las restricciones de la tabla "empleados" Muestra las 4 restricciones: 2 de control (1 habilitada y la otra no, no validan datos existentes), 1 "primary key" (habilitada y no valida datos existentes) y 1 única (habilitada y valida los datos anteriores).

```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
## Deshabilite todas las restricciones de "empleados"

```sql
ALTER TABLE empleados DISABLE CONSTRAINT ALL;

```
## Ingrese un registro que viole todas las restricciones.

```sql
INSERT INTO empleados VALUES (5, '77777777', 'Eduardo Escobar', 'Recursos', -5000);

```
## Habilite la restricción "CK_empleados_sueldo_positivo" sin validar los datos existentes.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT CK_empleados_sueldo_positivo NOVALIDATE;

```
## Habilite la restricción "CK_empleados_seccion_lista" sin validar los datos existentes.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT CK_empleados_seccion_lista NOVALIDATE;

```
## Intente habilitar la restricción "PK_empleados_codigo" sin validar los datos existentes.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo NOVALIDATE;

```
## Intente habilitar la restricción "UQ_empleados_documento" sin validar los datos existentes.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT UQ_empleados_documento NOVALIDATE;

```
## Elimine el registro que infringe con las restricciones "primary key" y "unique".

```sql
DELETE FROM empleados WHERE codigo = 4;

```
## Habilite las restricciones "PK_empleados_codigo" y "UQ_empleados_documento" sin validar los datos existentes.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo NOVALIDATE;
ALTER TABLE empleados ENABLE CONSTRAINT UQ_empleados_documento NOVALIDATE;

```
## Consulte el catálogo "user_constraints" y analice la información.

```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
