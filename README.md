# Yuguiro  Jorge Suni Benito.

# Curso: Database Programming with SQL

[1. Crear tablas (create table - describe - all_tables - drop table)](solucionario/solucionario-1.md)

[2. Ingresar registros (insert into- select)](solucionario/solucionario-2.md)

[3. Tipos de datos](solucionario/solucionario-3.md)

[4. Recuperar algunos campos (select)](solucionario/solucionario-4.md)

[5. Recuperar algunos registros (where)](solucionario/solucionario-5.md)

[6. Operadores relacionales](solucionario/solucionario-6.md)

[7. Borrar registros (delete)](solucionario/solucionario-7.md)

[8. Actualizar registros (update)](solucionario/solucionario-8.md)

[9. Comentarios](solucionario/solucionario-9.md)

[10. Valores nulos (null)](solucionario/solucionario-10.md)

[11. Operadores relacionales (is null)](solucionario/solucionario-11.md)

[12. Clave primaria (primary key)](solucionario/solucionario-12.md)

[13. Vaciar la tabla (truncate table)](solucionario/solucionario-13.md)

[14. Tipos de datos alfanuméricos](solucionario/solucionario-14.md)

[15. Tipos de datos numéricos](solucionario/solucionario-15.md)

[16. Ingresar algunos campos](solucionario/solucionario-16.md)

[17. Valores por defecto (default)](solucionario/solucionario-17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](solucionario/solucionario-18.md)

[19.  Alias (encabezados de columnas)](solucionario/solucionario-19.md)

[20. Funciones string](solucionario/solucionario-20.md)

[21. Funciones matemáticas](solucionario/solucionario-21.md)

[22. Funciones de fechas y horas](solucionario/solucionario-22.md)

[23. Ordenar registros (order by)](solucionario/solucionario-23.md)

[24. Operadores lógicos (and - or - not)](solucionario/solucionario-24.md)

[25. Otros operadores relacionales (between)](solucionario/solucionario-25.md)

[26. Otros operadores relacionales (in)](solucionario/solucionario-26.md)

[27. Búsqueda de patrones (like - not like)](solucionario/solucionario-27.md)

[28. Contar registros (count)](solucionario/solucionario-28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](solucionario/solucionario-29.md)

[30. Agrupar registros (group by)](solucionario/solucionario-30.md)

[31. Seleccionar grupos (Having)](solucionario/solucionario-31.md)

[32. Registros duplicados (Distinct)](solucionario/solucionario-32.md)

[33. Clave primaria compuesta](solucionario/solucionario-33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](solucionario/solucionario-34.md)

[35. Alterar secuencia (alter sequence)](solucionario/solucionario-35.md)

[36. Integridad de datos](solucionario/solucionario-36.md)

[37. Restricción primary key](solucionario/solucionario-37.md)

[38. Restricción unique](solucionario/solucionario-38.md)

[39. Restriccioncheck](solucionario/solucionario-39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](solucionario/solucionario-40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](solucionario/solucionario-41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](solucionario/solucionario-42.md)

[43. Indices](solucionario/solucionario-43.md)

[44. Indices (Crear . Información)](solucionario/solucionario-44.md)

[45. Indices (eliminar)](solucionario/solucionario-45.md)

[46. Varias tablas (join)](solucionario/solucionario-46.md)

[47. Combinación interna (join)](solucionario/solucionario-47.md)

[48. Combinación externa izquierda (left join)](solucionario/solucionario-48.md)

[49. Combinación externa derecha (right join)](solucionario/solucionario-49.md)

[50. Combinación externa completa (full join)](solucionario/solucionario-50.md)

[51. Combinaciones cruzadas (cross)](solucionario/solucionario-51.md)

[52. Autocombinación](solucionario/solucionario-52.md)

[53. Combinaciones y funciones de agrupamiento](solucionario/solucionario-53.md)

[54. Combinar más de 2 tablas](solucionario/solucionario-54.md)

[55. Otros tipos de combinaciones](solucionario/solucionario-55.md)

[56. Clave foránea](solucionario/solucionario-56.md)

[57. Restricciones (foreign key)](solucionario/solucionario-57.md)

[58. Restricciones foreign key en la misma tabla](solucionario/solucionario-58.md)

[59. Restricciones foreign key (eliminación)](solucionario/solucionario-59.md)

[60. Restricciones foreign key deshabilitar y validar](solucionario/solucionario-60.md)

[61. Restricciones foreign key (acciones)](solucionario/solucionario-61.md)

[62. Información de user_constraints](solucionario/solucionario-62.md)

[63. Restricciones al crear la tabla](solucionario/solucionario-63.md)

[64. Unión](solucionario/solucionario-64.md)

[65. Intersección](solucionario/solucionario-65.md)

[66. Minus](solucionario/solucionario-66.md)

[67. Agregar campos (alter table-add)](solucionario/solucionario-67.md)

[68. Modificar campos (alter table - modify)](solucionario/solucionario-68.md)

[69. Eliminar campos (alter table - drop)](solucionario/solucionario-69.md)

[70.  Agregar campos y restricciones (alter table)](solucionario/solucionario-70.md)

[71. Subconsultas](solucionario/solucionario-71.md)

[72. Subconsultas como expresion](solucionario/72solucionario-72.md)

[73. Subconsultas con in](solucionario/solucionario-73.md)

[74. Subconsultas any- some - all](solucionario/solucionario-74.md)

[75. Subconsultas correlacionadas](solucionario/solucionario-75.md)

[76. Exists y No Exists](solucionario/solucionario-76.md)

[77. Subconsulta simil autocombinacion](solucionario/solucionario-77.md)

[78. Subconsulta conupdate y delete](solucionario/solucionario-78.md)

[79. Subconsulta e insert](solucionario/solucionario-79.md)

[80. Crear tabla a partir de otra (create table-select)](solucionario/solucionario-80.md)

[81. Vistas (create view)](solucionario/solucionario-81.md)

[82. Vistas (información)](solucionario/solucionario-82.md)

[83. Vistas eliminar (drop view)](solucionario/solucionario-83.md)

[84. Vistas (modificar datos a través de ella)](solucionario/solucionario-84.md)

[85. Vistas (with read only)](solucionario/solucionario-85.md)

[86. Vistas modificar (create or replace view)](solucionario/solucionario-86.md)

[87. Vistas (with check option)](solucionario/solucionario-87.md)

[88. Vistas (otras consideraciones: force)](solucionario/solucionario-88.md)

[89. Vistas materializadas (materialized view)](solucionario/solucionario-89.md)

[90. Procedimientos almacenados](solucionario/solucionario-90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](solucionario/solucionario-91.md)

[92. Procedimientos Almacenados (eliminar)](solucionario/solucionario-92.md)

[93. Procedimientos almacenados (parámetros de entrada)](solucionario/solucionario-93.md)

[94. Procedimientos almacenados (variables)](solucionario/solucionario-94.md)

[95. Procedimientos Almacenados (informacion)](solucionario/solucionario-95.md)

[96. Funciones](solucionario/solucionario-96.md)

[97. Control de flujo (if)](solucionario/solucionario-97.md)

[98. Control de flujo (case)](solucionario/solucionario-98.md)

[99. Control de flujo (loop)](solucionario/solucionario-99.md)

[100. Control de flujo (for)](solucionario/solucionario-100.md)

[101. Control de flujo (while loop)](solucionario/solucionario-100.md)

[102. Disparador (trigger)](solucionario/solucionario-100.md)

[103. Disparador (información)](solucionario/solucionario-100.md)

[104. Disparador de inserción a nivel de sentencia](solucionario/solucionario-100.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](solucionario/solucionario-100.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](solucionario/solucionario-100.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](solucionario/solucionario-100.md)

[108. Disparador de actualización a nivel de fila (update trigger)](solucionario/solucionario-100.md)

[109. Disparador de actualización - lista de campos (update trigger)](solucionario/solucionario-100.md)

[110. Disparador de múltiples eventos](solucionario/solucionario-100.md)

[111. Disparador (old y new)](solucionario/solucionario-100.md)

[112. Disparador condiciones (when)](solucionario/solucionario-100.md)

[113. Disparador de actualizacion - campos (updating)](solucionario/solucionario-100.md)

[114. Disparadores (habilitar y deshabilitar)](solucionario/solucionario-100.md)

[115. Disparador (eliminar)](solucionario/solucionario-100.md)
